
<!-- README.md is generated from README.Rmd. Please edit that file -->

# holidayEstR

<!-- badges: start -->
<!-- badges: end -->

## Installation

You can install the development version of holidayEstR via:

``` r
remotes::install_gitlab("epidemics-r/holidayestr")
```

## Example

Calculate approximate school vacation days for given year(s):

``` r
library(holidayEstR)

vacation_days(2019)
#> # A tibble: 365 × 2
#>    date       value
#>    <date>     <dbl>
#>  1 2019-01-01     1
#>  2 2019-01-02     1
#>  3 2019-01-03     1
#>  4 2019-01-04     1
#>  5 2019-01-05     1
#>  6 2019-01-06     1
#>  7 2019-01-07     0
#>  8 2019-01-08     0
#>  9 2019-01-09     0
#> 10 2019-01-10     0
#> # ℹ 355 more rows

vacation_days(2021:2023)
#> # A tibble: 1,095 × 2
#>    date       value
#>    <date>     <dbl>
#>  1 2021-01-01     1
#>  2 2021-01-02     1
#>  3 2021-01-03     1
#>  4 2021-01-04     0
#>  5 2021-01-05     0
#>  6 2021-01-06     0
#>  7 2021-01-07     0
#>  8 2021-01-08     0
#>  9 2021-01-09     0
#> 10 2021-01-10     0
#> # ℹ 1,085 more rows
```
